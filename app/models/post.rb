class Post < ActiveRecord::Base
  has_many :comments, dependent: :destroy
  validates_presence_of :title
  validates_presence_of :body, :on => :create, :message => "can't be blank"
end
